" ABOUT THIS VIMRC {{{
" Author: Baskoro Indrayana <baskoroindrayana@protonmail.com>
" Source: (coming soon)
" }}}

" FIRST IMPORTANT SETTINGS {{{

set nocompatible

" }}}

" SYNTAX AND FILETYPES {{{

" enable loading filetype plugins
filetype plugin on

" load filetype-specific indent files
filetype indent on

" enable syntax processing 
syntax on

" set syntax to markdown for `.md` files
autocmd BufNewFile,BufRead *.md		set syntax=markdown

" }}}

" PLUGINS (`vim-plug`) {{{

call plug#begin('~/.vim/plugged')

" For key mappings, settings, etc., refer to the below plugin's github links
" (Just use the string below as-is)

Plug 'vim-airline/vim-airline'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdcommenter'
Plug 'othree/yajs.vim'
Plug 'mattn/emmet-vim'

call plug#end()

" Airline settings
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

" NERDTree settings

" Toggle NERDTree
map <C-n> :NERDTreeToggle<CR>

" Start NERDTree if no files are specified in previous vim command
augroup nerdtree_no_files
    autocmd StdInReadPre * let s:std_in=1
    autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
augroup END

" NERD Commenter

" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Set language(s) to use its alternate delimiters by default
let g:NERDAltDelims_java = 1

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

" vim-javascript settings

" Enable syntax HL for JSDocs
let g:javascript_plugin_jsdoc = 1

" }}}

" UI SETTINGS {{{
" badwolf (by Steve Losh)
colorscheme badwolf

" relative line numbering (for ease in motioning)
set relativenumber

" number of spaces to represent tabs
set tabstop=4

" number of spaces a tab counts WHEN editing
set softtabstop=4

" number of spaces for indentation
set shiftwidth=4

" convert tabs to spaces
set expandtab

" highlight current line
set cursorline 

" redraw only when needed
set lazyredraw

" highlight matching
set showmatch

" display characters for tabs, EOL's, etc.
set list
set listchars=tab:▸\ ,eol:¬,extends:❯,precedes:❮

" language-specific settings
autocmd Filetype ruby setlocal expandtab tabstop=2 softtabstop=2 shiftwidth=2
autocmd Filetype eruby setlocal expandtab tabstop=2 softtabstop=2 shiftwidth=2

" }}}

" SEARCHING {{{

" search as characters are entered
set incsearch

" highlight matches
set hlsearch

" turn off search highlighting 
nnoremap <leader><space> :nohlsearch<CR>

" }}}

" FOLDING {{{

" enable folding
set foldenable

" allow folding up to 10 levels
set foldnestmax=10

" space key to open/close fold
nnoremap <space> za

" folding method (fold-syntax by default)
augroup folding_by_filename
    autocmd!
    autocmd FileType * setlocal foldmethod=syntax
    autocmd FileType vim setlocal foldmethod=marker
augroup END

" }}}

" MOVEMENT {{{

" to be able to move up and down through wrapped text
nnoremap j gj
nnoremap k gk

" 'auto-center' cursor
nnoremap G Gzz
nnoremap n nzz
nnoremap N Nzz
nnoremap } }zz
nnoremap { {zz

" }}}

" LEADER SHORTCUTS {{{

" change <leader> key to specified character
let mapleader="\\"

" editing .vimrc / .bashrc
nnoremap <leader>ev :edit ~/.vimrc<CR>
nnoremap <leader>eb :edit ~/.bashrc<CR>
nnoremap <leader>sv :source ~/.vimrc<CR>

" tabs management
nnoremap <leader>h :bprevious<CR>
nnoremap <leader>l :bnext<CR>
nnoremap <leader>T :tabnew<CR>
nnoremap <leader>q :bdelete<CR>
nnoremap <leader>qf :bdelete!<CR>

" save current session (open tabs, etc.)
nnoremap <leader>s :mksession!<CR>

" }}}

" CLIPBOARD SETTINGS {{{

set clipboard=unnamedplus " copy/paste from/to system clipboard

" }}}
