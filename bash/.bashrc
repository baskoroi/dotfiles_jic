#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# add PATH env vars (if any)

# ruby gems
export PATH="${PATH}:/home/baskoroi/.gem/ruby/2.4.0/bin"

# go binaries
export PATH="${PATH}:/home/baskoroi/usr/local/go/bin"

# colors
COLOR_RED="\033[0;31m"
COLOR_YELLOW="\033[0;33m"
COLOR_GREEN="\033[0;32m"
COLOR_OCHRE="\033[38;5;95m"
COLOR_BLUE="\033[0;34m"
COLOR_WHITE="\033[0;37m"
COLOR_RESET="\033[0m"

# to show git status and branch

function git_color {
  local git_status="$(git status 2> /dev/null)"

  if [[ ! $git_status =~ "working directory clean" ]]; then
    echo -e $COLOR_RED
  elif [[ $git_status =~ "Your branch is ahead of" ]]; then
    echo -e $COLOR_YELLOW
  elif [[ $git_status =~ "nothing to commit" ]]; then
    echo -e $COLOR_GREEN
  else
    echo -e $COLOR_OCHRE
  fi
}

function git_branch {
  local git_status="$(git status 2> /dev/null)"
  local on_branch="On branch ([^${IFS}]*)"
  local on_commit="HEAD detached at ([^${IFS}]*)"

  if [[ $git_status =~ $on_branch ]]; then
    local branch=${BASH_REMATCH[1]}
    echo "($branch)"
  elif [[ $git_status =~ $on_commit ]]; then
    local commit=${BASH_REMATCH[1]}
    echo "($commit)"
  fi
}

# terminal prompt
PS1="\[\e[0;34m\]\u\[\e[m\] in \[\e[0;36m\]\w\[\e[m\]:"
PS1+="\[\$(git_color)\]"
PS1+=" \$(git_branch)"
PS1+="\n\[\e[0;33m\]\$\[\e[m\] "
#PS1='\u at \h in \W \$ '

# aliases
alias l='ls --color=auto'
alias ls='ls --color=auto'
alias ll='ls -la --color=auto'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias g='git'
alias e='nvim'
alias editi3='nvim ~/.config/i3/config'
alias keepass='keepass ~/m/.keys/BaskKey.kdbx'
alias redshift='redshift -l -6.263985:106.656740'
alias irssi='irssi -c irc.freenode.net -n bask' # append with `-w some_password`
alias pkg='cd ~/m/pkg'
alias books='cd ~/m/books'
alias devel='cd ~/m/devel'
alias dotf='cd ~/dotfiles'

# to pipe output to pastebin
# Usage: <command> | ptpb
# Usage 2: ptpb < <file>
alias ptpb='curl -F c=@- https://ptpb.pw'

# use urxvt for terminals
export TERM=rxvt-unicode

# export nvim as editor
export VISUAL=nvim
export EDITOR=nvim
